A fine data mangler directed for building intersected graphs.

A building flow will look like this:
 1. get raw data from sources (mongodb in current case)
 
 2. clustering the data (redis sets) by key (e.g: country:USA, country:*)  example for such a sets
 
   1. country:AUS =>
   
                   1. sspId-1
                   
                   2. sspId-2
                   

   2. minWidth:320x180 =>
   
                   1. sspId-2
                   
                   2. sspId-4
 
 
   3. ***GENERAL REP (no cap)
   
   
     1. country:* =>
     
                   1. sspId-5
                   
                   2. sspId-2
                   
                   

 3. UNION the general/unrelated sets with the related onces e.g
 
    `
    SUNIONSTORE country:openAndAUS country:* country:AUS
    `

 ** now one can intersect the sets in order to get the relevant ids

  `
  SINTER country:openAndAUS  widthXheight:*
  `
  
All rights reserved. DMG LTD . by Nir D Shabo
