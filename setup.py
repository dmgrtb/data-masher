#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of data-masher.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2017, NirD Shabo <nird@nrdlabs.com>

from setuptools import setup, find_packages
from data_masher import __version__

tests_require = [
    'mock',
    'nose',
    'coverage',
    'yanc',
    'preggy',
    'tox',
    'ipdb',
    'coveralls',
    'sphinx',
]

setup(
    name='data-masher',
    version=__version__,
    description='An incrdible redis index builder',
    long_description='''
An incrdible redis index builder
''',
    keywords='dmg,rtb,redis,data,masher',
    author='NirD Shabo',
    author_email='nird@nrdlabs.com',
    # url='https://github.com/someuser/somepackage',
    license='MIT',
    classifiers=[
        'Development Status ::  Beta',
        'Intended Audience :: DMG LTD',
        # 'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Operating System :: OS Independent',
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
    'pymongo>=3.1.1',
    'redis>=2.10.6'
        # add your dependencies here
        # remember to use 'package-name>=x.y.z,<x.y+1.0' notation (this way you get bugfixes)
    ],
    extras_require={
        'tests': tests_require,
    },
    entry_points={
        'console_scripts': [
            # add cli scripts here in this form:
            # 'data-masher=data_masher.cli:main',
        ],
    },
)
