class Scorer {
	init = require("rtb-common")("init");
	config = init.config("env");
	flatten = require('flat');
  var Sugar = require('sugar');

	constructor() {
		// this._pipeline =
		this.request_mapping_dict = {
			"sspId": "sspId",
			"app_black": "blacklist.app.id",
			"supplySource_black": "blacklist.sspId",
			"supplySource": "sspId",
			"country": "device.geo.country",
			"carrier": "device.carrier",
			"widthXheight": "widthXheight",
			"connectionType": "device.connectiontype",
			"os": "device.os",
			"mobileOs": "device.os",
			"isp": "device.isp",
			"app": "app.id",
			"devicetype": "device.devicetype",
			"osVersion": "device.osv",
			"osv": "device.osv",
			"ip": "device.ip"
		}
		this.inverse_request_mapping_dict = {
			"sspId": ["sspId", "supplySource"],
			"blacklist.app.id": "app_black": ,
			"blacklist.sspId": "supplySource_black",
			"device.geo.country": "country",
			"device.carrier": "carrier",
			"widthXheight": "widthXheight",
			"device.connectiontype":"connectionType",
			"device.os":"os",
			"device.os":"mobileOs",
			"device.isp":"isp",
			"app": "app.id",
			"devicetype": "device.devicetype",
			"osVersion": "device.osv",
			"osv": "device.osv",
			"ip": "device.ip"
		}

	}
	finishScoring(pipeline,creatives,supplySources) {
    var creative_item = creatives[Math.floor(Math.random()*items.length)];
    var supply_item = supplySources[Math.floor(Math.random()*items.length)];

    return self.redisClient.mget([creative_item,supply_item])
	}
  loweredArray(arr){
    arr = [];
    for (var i = 0; i < arr.length; i++) {
      var str = arr[i];
      arr.push(str.toLowerCase())
    }
    return arr;
  }
	matchTargeting(target, requestObj = {}) {
    pipeline = redisClient.multi();
		normalized_request = flatten(requestObj);
    normalized_request_keys= loweredArray(Object.keys(normalized_request));
    inverse_request_mapping_dict_keys = Object.keys();
    relevant_target_keys = Sugar.Array.unique(normalized_request_keys.intersect(inverse_request_mapping_dict_keys));
    var blacklisted =[];
    var finalCheck = [];
    var permuts = [];
    var country = [];
    randTime =  new Date();
    for (var i = 0; i < relevant_target_keys.length; i++) {
      var relevant_key=relevant_target_keys[i];
      var req_val=(normalized_request[relevant_key][0]);
      var searchTerm = relevant_key + "." + Sugar.parameterize(req_val)
      finalCheck.push(searchTerm);
      if(searchTerm.indexOf("geo.country") >-1){
        country.push(searchTerm);
      }else{
        permuts.push(searchTerm);
      }
      blacklisted.push("blacklist."+searchTerm)
    }
    for (var i = 0; i < permuts.length; i++) {
      var z=    permuts[i];
      // # searching something good...
  		pipeline.zrangebylex("idx:keys",('['+country[0]+'::'+(z)) +'::\\xff',('['+country[0]+'::'+(z)) +"\\xff")
    }
    pipeline.execute(function(data){

      console.log(data);
      if(data){
        res = data;
        flat_list= Sugar.unique(Sugar.flatten(res));
        pipeline.sunionstore(randTime,flat_list);
        pipeline.sdiff(randTime,blacklisted);
        pipeline.zrangebyscore("bids",1.0*normalized_request["imp"][0][0]["bidfloor"],"+inf");
        pipeline.delete(randTime);
        pipeline.execute(function(execu){
          // last handling of the data
        var final = Sugar.unique(execu[1].intersects(execu[2]))
        var creatives =[];
        var supplySources = [];
        for (var i = 0; i < final.length; i++) {

          item = final[i];
          if (item.indexOf("creative")){
            creatives.push(item);
          }
          if (item.indexOf("supply")){
            creatives.push(item);
          }
        }

      		  scored = finish_scoring(pipeline,supplySources,creatives)
      		  console.log(scored)

      		  return  scored
        });
      }
    })



	}

}
var scorer = new Scorer()
scorer.matchTargeting()
