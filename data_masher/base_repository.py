#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
from slugify import slugify
from pymongo import MongoClient
from rediscluster import StrictRedisCluster
class BaseRepository:
# ssp id
# type
# size
# countries
# mobile os
# desktop os
# connection type
# domains
# apps
# apps_black -> XXXX

    """docstring for BaseRepository"""
    def __init__(self, redisHosts=[{"host": 'docker', "port": 7000}], mongoConnectionString='mongodb://docker:27017/rtb'):
        self.redisClient = StrictRedisCluster(startup_nodes=redisHosts)
        self.mongoClient = MongoClient(mongoConnectionString)
        self.SEPERATOR="."
        # key is the shortname value is the fullname
        self.request_mapping_dict = {
        "sspId":"sspId",
        "app_black":"blacklist.app.id",
        "supplySource_black":"blacklist.sspId",
        "supplySource":"sspId",
        "country": "device.geo.country",
        "carrier": "device.carrier",
        "widthXheight":"widthXheight",
        "connectionType": "device.connectiontype",
        "os":"device.os",
        "mobileOs":"device.os",
        "isp": "device.isp",
        "app":"app.id",
        "devicetype": "device.devicetype",
        "osVersion": "device.osv",
        "osv":"device.osv",
        "ip":"device.ip"
        }
        self.inverse_request_mapping_dict = dict()
        for key in self.request_mapping_dict.keys():
            value_to_check = self.request_mapping_dict.get(key)
            values = ([x for x in self.request_mapping_dict.keys() if self.request_mapping_dict[x] is value_to_check])


            value = set(values)
            if type(value) is set:
                    self.inverse_request_mapping_dict[value_to_check]=value
            else:
                self.inverse_request_mapping_dict[value]= key

    def get_data_cursor(self, dbName="rtb", collectionName="SSPList", query={"status": "active"}):
        db = self.mongoClient[dbName]
        cursor = db[collectionName].find(query)
        return cursor

    def get_full_key_path(self,keyName,SEPERATOR="."):
        return self.request_mapping_dict.get(keyName,None)



    @staticmethod
    def index_name_to_key_value(indexName,SEPERATOR="."):
        redisClient = redis.StrictRedis()
        ind2remove=["idx:"]
        # print indexName
        terms=indexName.replace("idx:","").split(SEPERATOR)
        value=str(redisClient.smembers(indexName))
        ind2remove.append(value)
        a = ([x for i,x in enumerate((terms)) if x not in ind2remove])
        key=SEPERATOR.join(a)
        b = dict()
        b[key]=value
        return b
