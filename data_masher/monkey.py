#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of data-masher.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2017, NirD Shabo <nird@nrdlabs.com>
import redis
from slugify import slugify
from pymongo import MongoClient
from base_repository import BaseRepository
from rediscluster import StrictRedisCluster

class Monkey(BaseRepository):
    @staticmethod
    def get_valid_request():
        return {"app":{"storeurl":"http://tubemate.net","domain":"tubemate.net","cat":["IAB1"],"name":"TubeMate YouTube Downloader","publisher":{"id":"9665"},"id":"211469","bundle":"http://tubemate.net"},"cur":["USD"],"at":2,"tmax":300,"bcat":["IAB24","IAB25","IAB26"],"id":"CF5BE5A0D851786E85E0B91E4D389D09","imp":[{"ext":{"brandsafe":1,"mraid":1},"displaymanager":"mobfox_sdk","displaymanagerver":"5.2.1","bidfloor":0.073281,"banner":{"battr":[8,10],"topframe":0,"pos":0,"w":320,"btype":[4],"h":50,"wmax":320,"id":"1","api":[3],"hmax":50},"bidfloorcur":"USD","id":"1","secure":0,"instl":0}],"device":{"geo":{"country":"ita","lon":98.9786,"type":2,"lat":9.2389},"carrier":"Total Access Communication PLC","osv":"5.1","os":"Windows","ip":"1.47.135.206","js":1,"model":"SM-J200GU","dnt":0,"ua":"Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-J200GU Build/LMY47X)","connectiontype":3,"make":"Samsung","devicetype":4,"isp":"Total Access Communication PLC"},"user":{"id":""},"badv":[],"sspId":"59e7142bcbddd721def78c1e","adType":"Display","dateTime":"2017-04-12T07:56:38.813Z","sspTrafficSourceId":1401,"sspName":"Mobfox"}
