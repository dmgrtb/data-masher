#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of data-masher.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2017, NirD Shabo <nird@nrdlabs.com>
import redis
from slugify import slugify
import time
from timeit import default_timer as timer

import itertools
import pandas as pd
import random
import numpy as np
from base_repository import BaseRepository
from pandas.io.json import json_normalize
from pymongo import MongoClient
from rediscluster import StrictRedisCluster


class Scorer(BaseRepository):

    def __init__(self, redisHosts=[{"host": 'localhost', "port": 6379}], mongoConnectionString='mongodb://docker:27017/rtb'):
        BaseRepository.__init__(self,redisHosts,mongoConnectionString)


    #  now its just picking random....
    def finish_scoring(self,pipeline,ads):
        ov=set(random.choice(ads))
        return self.redisClient.mget(ov)


    def match_targeting(self,target=None,requestObj={}):
        SEPERATOR=self.SEPERATOR
        start = timer()
        pipeline = self.redisClient.pipeline(True)
        # this one is flattening json objet tree to dot key name flat object. e.g {"person":{"firstname":"nir shabo"}} becomes {"person.firstname":"nir shabo"}
        normalized_panda=json_normalize(requestObj,sep=SEPERATOR)
        normalized_request=normalized_panda.to_dict()
        # getting only keys i can target in the system . see BaseRepository for represntation details
        relevant_target_keys = set([item.lower() for item  in normalized_request.keys()]) &  set(self.inverse_request_mapping_dict.keys())
        # targeting list
        finalCheck = list()
        #blacklisted
        blacklisted = list()
        # random key for temporary intersects/unions for this request
        randTime =  "::".join(relevant_target_keys)+"_"+str(time.time())
        blacklisted.insert(0,randTime)
        for relevant_key in relevant_target_keys:
            req_val=str(normalized_request.get(relevant_key).get(0))
            searchTerm = relevant_key + "." + slugify(req_val)
            finalCheck.append(searchTerm)
            blacklisted.append("blacklist."+searchTerm)
        # lazy dirty way to get the country param beside the rest
        permuts=[x for x in finalCheck if "device.geo.country" not in x]
        country= [x for x in finalCheck if "device.geo.country" in x ]
        for z in list((permuts)):
            # searching something good...
            pipeline.zrangebylex("idx:keys",('['+country[0]+'::'+(z)) +'::\\xff',('['+country[0]+'::'+(z)) +"\\xff")
        res=pipeline.execute()
        flat_list = set([item for sublist in res for item in sublist if len(sublist)>0])
        # unions the flatted search results
                
        pipeline.sunionstore(randTime,*flat_list)
        pipeline.sdiff(randTime,blacklisted)
        pipeline.zrangebyscore("bids",float(normalized_panda.get("imp")[0][0]["bidfloor"]),"+inf")
        pipeline.delete(randTime)
        execu=pipeline.execute()
        final=sorted(set(execu[1])&set(execu[2]))
        creatives = [x for x in final if "creative" in x]
        supplySources = [x for x in final if "supply" in x]
        scored = self.finish_scoring(pipeline,zip(supplySources,creatives))
        print(scored)
        print("scoring time: " + str((timer()-start)*1000) + " MILISECS")
        return  scored



    @staticmethod
    def get_targeting_index_set():
        redisClient = redis.StrictRedis()
        return  redisClient.zrange("idx:keys",0,-1)
