#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of data-masher.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2017, NirD Shabo <nird@nrdlabs.com>

from data_masher.version import __version__  # NOQA
# from data_masher.masher import Masher  # NOQA
SEPERATOR=":"
