#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of data-masher.

# Copyright (c) 2017, DSNRMG LTD by NirD Shabo <nird@nrdlabs.com>
import redis
import time
from pandas.io.json import json_normalize
import numpy as np
from slugify import slugify
from pymongo import MongoClient
from rediscluster import StrictRedisCluster
from base_repository import BaseRepository
import itertools
from timeit import default_timer as timer
class Masher(BaseRepository):

  def __init__(self, redisHosts=[{"host": 'localhost', "port": 6379}], mongoConnectionString='mongodb://docker:27017/rtb'):
      BaseRepository.__init__(self,redisHosts,mongoConnectionString)
      cursor = self.get_data_cursor(collectionName="countries", query=None)
      self.countries = []
      for country in cursor:
          self.countries.append(country)

  def __parse_val__(self, value):
    target_value_type = type(value)
    if target_value_type is list:
        return [self.__parse_val__(i) for i in value if i]
    if target_value_type is dict:
        return self.__parse_val__(list(value.keys()))
    else:
        return slugify(str(value))

  def index_supply_source(self, target, bid):
    pipeline = self.redisClient.pipeline(True)
    redisClient = self.redisClient
    target_keys = list(target.keys())
    bidid = str(bid["_id"])
    maxBid=bid.get("maxBid",None)
    if maxBid is not None:
        pipeline.zadd("bids",float(maxBid),"supplySource_"+bidid)
    minBid=bid.get("minBid",None)
    if minBid is not None:
        pipeline.zadd("bids",float(minBid),"supplySource_"+bidid)
    pipeline.set("supplySource_"+bidid,bid)
    #open to the world baby
    allkeys=redisClient.zrange("idx:keys",0,-1)
    if len(target_keys) == 0:
        print(bidid+" is open to all")
        for k in allkeys:
            pipeline.sadd(k,"supplySource_"+bidid)
    else:
        for key in target.keys():
            nke = self.request_mapping_dict.get(key)
            tval=None
            if "app"  not in key:
                tval=target.get(key)
            else:
                tval = [k.keys() for k in target.get(key).values()][0]
            for item in itertools.permutations([nke+"."+x for x in [slugify(i) for i in tval]]):
                relevant_keys_for_supply=set(redisClient.zrangebylex("idx:keys","["+"::".join(item)+"::\\xff","["+"::".join(item)+"\\xff"))
                if len(relevant_keys_for_supply) == 0:
                    print("supplySource="+bidid+" has no matching creatives/campaigns!!!")
                else:
                    for k in relevant_keys_for_supply:
                        per = k.split("::")
                        for ke in itertools.permutations(per):
                            pipeline.sadd("::".join(ke),"supplySource_"+bidid)
    pipeline.execute()

  def index_creative(self, target, creativeid,cmpgn):
    SEPERATOR=self.SEPERATOR
    target_keys = list(target.keys())
    if len(target_keys) == 0:
        print("CREATIVE IS OPEN TO ALL",creativeid)
    indexKeys=[]
    pipeline = self.redisClient.pipeline(True)
    maxBid=cmpgn.get("maxBid",None)
    minBid=cmpgn.get("minBid",None)
    if maxBid is not None:
        pipeline.zadd("bids",float(maxBid),"creative_"+creativeid)
    if minBid is not None:
        pipeline.zadd("bids",float(minBid),"creative_"+creativeid)
    pipeline.set("creative_"+creativeid,cmpgn)
    final_target_object = dict()
    flat=list()
    for key in target_keys:
      tval=None
      if "app"  not in key:
          tval=target.get(key)
      else:
          tval = [k.keys() for k in target.get(key).values()][0]
      target_val = self.__parse_val__(tval)
      nke = self.request_mapping_dict.get(key)
      if nke is None:
        print('key='+key +"=None")
      for v in target_val:
          indexKey="idx:" + str(nke) + self.SEPERATOR +(v)
          if final_target_object.get(nke) is None:
              final_target_object[nke]=list()
          final_target_object[nke].append(v)
    target_set=list(itertools.chain([(key,set([kv for kv in final_target_object.get(key)])) for key in sorted(final_target_object.keys())]))
    t=dict()
    for target_kv in target_set:
        t[target_kv[0]]=[target_kv[0]+"."+str(kval) for kval in target_kv[1]]
    permutationsWeNeedToBuild = list(itertools.permutations(t.keys()))
    for permutata in permutationsWeNeedToBuild:
        items=zip(*[t[p] for p in permutata])
        iterabels=itertools.permutations(items)
        for per in iterabels:
            for pex in per:
                if "country" not in t.keys() and "device.geo.country" not in t.keys():
                    for ci in list(["device.geo.country."+slugify(str(country["alpha-3"])) for country in self.countries]):
                        pe = "::".join((ci,)+pex)
                        pipeline.sadd(pe,"creative_"+creativeid)
                        self.add_index(pipeline,pe)
                else:
                    pe = "::".join(pex)
                    pipeline.sadd(pe,"creative_"+creativeid)
                    self.add_index(pipeline,pe)
    pipeline.execute()

  def add_index(self,pipeline,indexNames):
      if type(indexNames) is list:
          for i in indexNames:
              pipeline.zadd("idx:"+"keys",0,i)
      else:
          pipeline.zadd("idx:"+"keys",0,indexNames)

    # SSP
  def build_creative_targeting(self, departments=["mobile", "apps"]):
    cursor = self.get_data_cursor(dbName="rtb", collectionName="advertisers")
    for advertiser in cursor:
      for cmpgn in advertiser["campaigns"]:
        isAllSpent=False if  float(cmpgn["cap"])-float(cmpgn["spent"])>=0  else True
        if cmpgn["status"].lower() == "active" and cmpgn['department'].lower() in departments and not isAllSpent:
          target = cmpgn["target"]
          creative_ids_arr = cmpgn["creatives"]
          for cid in creative_ids_arr:
              self.index_creative(target, str(cid),cmpgn)

  def build_supply_targeting(self, supplyTypes=["display-rtb", "native"]):
    cursor = self.get_data_cursor(dbName="rtb", collectionName="SSPList")
    for ssp in cursor:
      isAllSpent = False if float(ssp["cap"])-float(ssp["spent"])>=0 else True
      if not isAllSpent:
          for bid in ssp["bids"]:
              if bid["status"] == "active" and bid['supplyType'] in supplyTypes:
                  target = bid["target"]
                  self.index_supply_source(target, bid)

# main entry method. calls rest of the builders
  def build_targeting(self):
    before=timer()
    self.build_creative_targeting()
    self.build_supply_targeting()
    print("targeting set building time : "+str((timer()-before)* 1000.0)) + " MILISECS"
