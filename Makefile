# This file is part of data-masher.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2017, NirD Shabo <nird@nrdlabs.com>

# lists all available targets
list:
	@sh -c "$(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'make\[1\]' | grep -v 'Makefile' | sort"
# required for list
no_targets__:

# install all dependencies (do not forget to create a virtualenv first)
setup:
	@pip install -U -e .\[tests\]

# test your application (tests in the tests/ directory)
test: mongo_test redis_test unit

unit:
	@coverage run --branch `which nosetests`   tests/
	@coverage report -m --fail-under=80

# show coverage in html format
coverage-html: unit
	@coverage html

# get a redis instance up (localhost:4444)
redis: kill_redis
	redis-server ./redis.conf; sleep 1
	redis-cli -p 4444 info > /dev/null

# kill this redis instance (localhost:4444)
kill_redis:
	-redis-cli -p 4444 shutdown

# get a redis instance up for your unit tests (localhost:4448)
redis_test: kill_redis_test
	@redis-server ./redis.tests.conf; sleep 1
	@redis-cli -p 4448 info > /dev/null

# kill the test redis instance (localhost:4448)
kill_redis_test:
	@-redis-cli -p 4448 shutdown

# get a mongodb instance up (localhost:3333)
mongo: kill_mongo
	@mkdir -p /tmp/data-masher/mongodata && mongod --dbpath /tmp/data-masher/mongodata --logpath /tmp/data-masher/mongolog --port 3333 --quiet &

# kill this mongodb instance (localhost:3333)
kill_mongo:
	@-ps aux | egrep -i 'mongod.+3333' | egrep  egrep | awk '{ print $$2 }' | xargs kill -9

# clear all data in this mongodb instance (localhost: 3333)
clear_mongo:
	@rm -rf /tmp/data-masher && mkdir -p /tmp/data-masher/mongodata

# get a mongodb instance up for your unit tests (localhost:3334)
mongo_test: kill_mongo_test

# kill the test mongodb instance (localhost: 3334)
kill_mongo_test:
	@-ps aux | egrep -i 'mongod.+3334' | egrep egrep | awk '{ print $$2 }' | xargs kill -9

# run tests against all supported python versions
tox:
	@tox

#docs:
	#@cd data_masher/docs && make html && open _build/html/index.html
